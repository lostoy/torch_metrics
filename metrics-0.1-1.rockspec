package = "metrics"
version = "0.1-1"
source = {
  url = "https://lostoy@bitbucket.org/lostoy/torch_metrics.git",
  branch = 'master'
}
dependencies = {
  "lua"
}
build = {
  type = "builtin",
  modules = {
    ['metrics'] = 'init.lua'
  }
}
